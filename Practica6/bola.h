#ifndef BOLA_H
#define BOLA_H
#include <QGraphicsItem>
#include <QPainter>


class Bola: public QGraphicsItem
{
private:
    double  Masa;
    double  Px;
    double  Py;
    double  Vx;
    double  Vy;
    double constG;
    double Cofriccion;
    double Radio;

public:
    Bola();
    QRectF boundingRect() const;
    void paint(QPainter *painter,
    const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mover();
    void choque();
    void choquex();
    void actualizar(double dt=0.1);
    double  get_px(){return Px;}
    double  get_py(){return Py;}
    double  get_masa(){return Masa;}
    double get_vx(){return Vx;}
    double get_vy(){return Vy;}
};

#endif // BOLA_H
