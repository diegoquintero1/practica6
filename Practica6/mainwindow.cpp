#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500);
    ui->graphicsView->setScene(scene);


    ui->graphicsView->setBackgroundBrush(QImage(":/Fondo shenlong.jpg"));

    timer=new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
    timer->start(20);

    l1=new QGraphicsLineItem(0,0,1000,0);
    l2=new QGraphicsLineItem(0,0,0,500);
    l3=new QGraphicsLineItem(1000,0,1000,500);
    l4=new QGraphicsLineItem(0,500,1000,500);
    scene->addItem(l1);
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);
}

MainWindow::~MainWindow()
{
    delete scene;
    delete ui;
}


void MainWindow::animar()
{

    for(int i=0; i<bolas.length();i++)
    {
        bolas.at(i)->mover();
        if (!bolas.at(i)->collidingItems().empty()){

            bolas.at(i)->choque();
            if (bolas.at(i)->collidesWithItem(l2)|| bolas.at(i)->collidesWithItem(l3)){
                bolas.at(i)->choquex();
            }

    }
    }
}

void MainWindow::mousePressEvent(QMouseEvent *ev)
{
    bolas.append(new Bola());
    scene->addItem(bolas.last());
    bolas.last()->setPos(ev->x(),ev->y());
}


