#include "bola.h"

Bola::Bola()
{
    Vy=7;
    Masa=70;
    Px=x();
    Py=y();
    Vx=7;
    constG=10;
    Cofriccion=0.01;
    Radio=5;

}

QRectF Bola::boundingRect() const
{
    return QRectF(-10,-10,20,20);
}

void Bola::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap pixmap;
    pixmap.load(":/bola.png");
    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
}
void Bola::actualizar(double dt){
    double angulo=atan2(Vy,Vx);
    double V=sqrt(pow(Vx,2)+pow(Vy,2));
    double ax=-Cofriccion*pow(V,2)*pow(Radio,2)/Masa*cos(angulo);
    double ay=-Cofriccion*pow(V,2)*pow(Radio,2)/Masa*sin(angulo)+constG;
    Px=Px+Vx*dt;
    Py=Py+Vy*dt+constG*dt*dt/2;
    Vy=Vy+ay*dt;
    Vx=Vx+ax*dt;
}

void Bola::mover()
{
    setPos(x()+Vx,y()+Vy);
    actualizar();
}

void Bola::choque()
{
    Vy=-Vy/1.3;

}
void Bola::choquex(){
    Vx=-Vx/1.3;
}
